<?php
namespace Builder;

interface BuilderInterface
	{

	public function build();
	public function getTitle();
	public function getColor();

	} 
?>