<?php
namespace Builder;
use Builder\BuilderInterface;
use Builder\Product;
	/**
	* Director
	*/
	class Director
	{
		private $product;

		public function __construct(BuilderInterface $builder)
		{
			$this->product = new Product();
			$this->product->setTitle($builder->getTitle());
			$this->product->setColor($builder->getColor());
		}
		public function getResult()
		{
			return $this->product;
		}
	}

?>