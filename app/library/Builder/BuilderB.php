<?php
namespace Builder;
use Builder\BuilderInterface;
	/**
	 * 
	 */
	class BuilderB implements BuilderInterface
	{
		private $title;
		private $color;

		public function setTitle($title){
			$this->title = $title;
			return $title;
		}
		public function setColor($color){
			$this->color = $color;
			return $color;
		}

		public function getTitle(){
			return $this->title;
		}

		public function getColor(){
			return $this->color;
		}
		public function build(){
			return new Director($this);
		}
	}

?>