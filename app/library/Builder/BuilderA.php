<?php
namespace Builder;
use Builder\BuilderInterface;
	/**
	* 
	*/
	class BuilderA implements BuilderInterface
	{
		private $title;
		private $color;

		public function setTitle($title){
			$this->title = $title;
		}
		public function setColor($color){
			$this->color = $color;
		}

		public function getTitle(){
			return $this->title;
		}

		public function getColor(){
			return $this->color;
		}
		public function build(){
			return new Director($this);
		}
	}

 ?>