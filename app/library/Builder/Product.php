<?php
namespace Builder;
	/**
	* Product
	*/
	class Product 
	{
		private $title;
		private $color;
		public function getTitle(){
			return $this->title;
		}
		public function setTitle($title){
			$this->title = $title;
		}

		public function getColor(){
			return $this->color;
		}

		public function setColor($color){
			$this->color = $color;
		}
	}
?>